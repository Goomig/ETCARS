#pragma once
#include "prerequisites.h"
class TransportLocation;
#include "TransportLocation.h"

class Job
{
private:
	static Job*  current;
public:

	bool wasSpeeding;
	bool jobStartedEventFired;
	bool loadedFromSave;
	int speedingCount;

	const char* steamID;
	const char* steamUsername;
	const char* steamDLCJSON;

	int status;//0 = new, 1 = in progress, 2 = finished, 3 = possibly cancelled?;

	const char* sourceCity;
	const char* sourceCompany;
	const char* destinationCity;
	const char* destinationCompany;
	const char* cargo;
	const char* truckMake;
	const char* truckModel;
	const char* game;

	const char* sourceCityID;
	const char* sourceCompanyID;
	const char* destinationCityID;
	const char* destinationCompanyID;
	const char* cargoID;
	const char* truckMakeID;
	const char* truckModelID;
	const char* gameID;
	const char* gameVersion;

	float topSpeed;
	unsigned int income;
	float trailerMass;
	float distanceDriven;
	float fuelBurned;
	float fuelPurchased;
	float startOdometer;
	float endOdometer;
	int timeRemaining;
	int timeStarted;
	int timeDue;
	unsigned int timeDelivered;
	int collisionCount;

	float finishTrailerDamage;
	float startTrailerDamage;

	float deliveryX;
	float deliveryY;
	float deliveryZ;

	float pickupX;
	float pickupY;
	float pickupZ;

	float trailerDeliveryX;
	float trailerDeliveryY;
	float trailerDeliveryZ;

	float trailerPickupX;
	float trailerPickupY;
	float trailerPickupZ;

	float startEngineDamage;
	float startTransmissionDamage;
	float startCabinDamage;
	float startChassisDamage;
	float startWheelDamage;

	float finishEngineDamage;
	float finishTransmissionDamage;
	float finishCabinDamage;
	float finishChassisDamage;
	float finishWheelDamage;



	float totalEngineDamage;
	float totalTransmissionDamage;
	float totalCabinDamage;
	float totalChassisDamage;
	float totalWheelDamage;
	float totalTrailerDamage;


	bool isMultiplayer;
	bool late;
	float navigationDistanceRemaining;
	bool onJob;
	bool wasFinished;
	bool wasTrailerDisconnnected;
	std::string pluginVersion;

	long long realTimeStarted;
	long long realTimeEnded;
	long long realTimeTaken;
	
	//LIVE PARAMETERS REQUIRED FOR MATHS
	float fuel;
	float odometer;
	float engineDamage;
	float transmissionDamage;
	float cabinDamage;
	float chassisDamage;
	float wheelDamage;
	float trailerDamage;
	
	Job();
	void Reset();
	static Job* Current(bool requireNew = false);

	~Job();
};
class StorageJob
{
public:

	bool wasSpeeding;
	int speedingCount;
	std::string steamID;

	const char* sourceCityID;
	const char* sourceCompanyID;
	const char* destinationCityID;
	const char* destinationCompanyID;
	const char* cargoID;
	const char* truckMakeID;
	const char* truckModelID;
	const char* cargoAccessoryID;
	std::vector<TransportLocation> transportLocs;
	float topSpeed;
	unsigned int income;
	float trailerMass;
	float distanceDriven;
	float fuelBurned;
	float fuelPurchased;
	float startOdometer;
	int collisionCount;

	float startTrailerDamage;
	float pickupX;
	float pickupY;
	float pickupZ;
	float trailerPickupX;
	float trailerPickupY;
	float trailerPickupZ;

	float startEngineDamage;
	float startTransmissionDamage;
	float startCabinDamage;
	float startChassisDamage;
	float startWheelDamage;

	float totalEngineDamage;
	float totalTransmissionDamage;
	float totalCabinDamage;
	float totalChassisDamage;
	float totalWheelDamage;
	float totalTrailerDamage;

	float lastOdometer;
	float lastFuel;

	int ETCARSMajorVersion;
	int ETCARSMinorVersion;

	long long realTimeStarted;
	long long realTimeTaken;
	long long timeStarted;

	float engineDamage;
	float transmissionDamage;
	float cabinDamage;
	float chassisDamage;
	float wheelDamage;
	float trailerDamage;

	bool configLoaded;

	StorageJob();
	void reset();
};