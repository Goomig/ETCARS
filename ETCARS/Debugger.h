#pragma once
#include "prerequisites.h"
namespace Debugger
{
	void init(scs_log_t log, const char* logFileName);
	void log(LogTo log, LogLevel level, const char* message);
	void log_line(const char* message);
	void finish_log();
	const char* currentTime();
	const char* logLevelToString(LogLevel level);
	static scs_log_t game_log = NULL;
	static FILE *log_file = NULL;
	static time_t now = time(0);
	static tm localTime;
	static char timeBuffer[4096];
	static char* timeNBuffer = "";
	static std::string _logLine;
}