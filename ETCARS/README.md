# ETCARS - Electronic Truck Communications Addressing and Reporting System

This project is designed to make it easier to retrieve data from Euro Truck Simulator 2 and American Truck Simulator by making all data accessible via events on
a local socket server. Clients can connect using a standard raw tcp connection on port 30001 and receive in-game data as well as other events.

## TODO:
	
* Add ferries
* Add trains
* Add tolls**
* General Cleanup
* Mac OS X Support**
* Create examples of how to connect and use this library**
* Testing

** - Low priority


## Getting Started

All that is needed is to have this project installed by downloading the installer from [here](https://etcars.menzelstudios.com).
NOTE: The site is not created yet, but we are working on it!

### Prerequisites

There are some requirements to run this software. The list is below:

* Legal copy of [Euro Truck Simulator 2](http://store.steampowered.com/app/227300/Euro_Truck_Simulator_2/) or [American Truck Simulator](http://store.steampowered.com/app/270880/American_Truck_Simulator/)
* [Microsoft Visual Studio C++ Redistributable 2015(MSVCR 14.1) x86](https://go.microsoft.com/fwlink/?LinkId=746571)
* [Microsoft Visual Studio C++ Redistributable 2015(MSVCR 14.1) x64](https://go.microsoft.com/fwlink/?LinkId=746572)
* [Steam](https://steamcommunity.com/)
* [Legal copy of Windows 7, Windows 8, Windows 8.1, Windows 10 with all updates installed](https://www.microsoft.com/en-us/windows/)
* Any version of linux

### Installation

Simply copy one of the released dlls[linux is so] to a game's plugins directory or run the installer.

File Paths:
* Windows 32-bit - steamapps/common/American Truck Simulator/bin/win_x86/plugins
* Windows 64-bit - steamapps/common/American Truck Simulator/bin/win_x64/plugins
* Linux 32-bit - ~/.steam/steam/steamapps/common/American Truck Simulator/bin/linux_x86/plugins
* Linux 64-bit - ~/.steam/steam/steamapps/common/American Truck Simulator/bin/linux_x64/plugins




### Connecting

To connect, make sure you have Euro Truck Simulator 2 or American Truck Simulator running with this plugin loaded and then
open a tcp connection to localhost:30001. An example of the settings needed for putty are given below:

Putty Example:
Address: localhost
Port: 30001
Type: Raw or Telnet

Socket.io: use the 'data' event

## Documentation

Currently no documentation of the data structures or events is available. Once the events and data have settled down a little bit more, we can then create the needed information!
For now, connecting with putty and using copy/paste into notpad++ will give you a good idea of what to expect.

NOTE: Some of the data is encoded in UTF-8, but for some languages, this is a problem and the UTF-8 fields need to be decoded to
windows-1252

##### Supported Platforms
* Windows 7(All editions) - 64 bit
* Windows 7(All editions) - 32 bit
* Windows 8(All editions) - 64 bit
* Windows 8(All editions) - 32 bit
* Windows 8.1(All editions) - 64 bit
* Windows 8.1(All editions) - 32 bit
* Windows 10(All editions) - 64 bit
* Windows 10(All editions) - 32 bit
* Linux - 32 bit
* Linux - 64 bit


##### Known Issues
* Job cancellation detection is experimental and may not work correctly.


## Built With

* [Boost C++ 1.66](http://www.boost.org/)
* [CryptoPP(Crypto++)](https://www.cryptopp.com/)
* [OpenSSL](https://www.openssl.org/)
* [Steamworks](https://partner.steamgames.com)
* [SCS SDK](http://modding.scssoft.com/wiki/Documentation/Engine/SDK/Telemetry)
* [cURL](https://curl.haxx.se)

## Contributing

Currently not open for contributions, although if you have a request, please either open an issue or email josh@menzelstudios.com!

## Versioning

Versions are currently set by myself, jammerxd and will be in the format of major(period)minor - ex. 0.1

## Authors

* **Josh Menzel** - *Primary Developer* - [jammerxd](https://gitlab.com/jammerxd)
* **Thomas Karlsen** - *Support and Development* - [TheUnknownNO](https://gitlab.com/thomasandre.karlsen)


See also the list of [contributors](https://gitlab.com/jammerxd/ETCARS/contributors) who participated in this project.

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details


## Changelog
[March 26, 2018]
-Changelog Created
-Job saving is being tested. Save files encrpyted using Crypto++, 1 save for single player, 1 save for multiplayer per game.
-NewMessage.h has been hidden to protect encryption/file saving algorithms.
-Linux is also continuing to undergo testing
-Windows is undergoing testing


## Acknowledgments

* Fabrice "The Fabby"
* Ben "Ratcho"
* "Engineer"
* "killer59800"
* dowmesiter